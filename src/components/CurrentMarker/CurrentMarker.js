import React from 'react';
import 'styles/CurrentMarker.css';

import { Marker, InfoWindow } from 'react-google-maps';

const CurrentMarker = (props) => {
    const {latitude, longitude, title, description} = props.currentMarker;
    if(!latitude){
        return null;
    }
    return <Marker icon="http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2%7C555555" position={{lng: longitude, lat: latitude}}></Marker>
}

export default CurrentMarker;
