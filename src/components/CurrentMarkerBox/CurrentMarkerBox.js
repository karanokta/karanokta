import React from 'react';

import { Input, Icon, Button } from 'antd';
import 'styles/CurrentMarkerBox.css';

const CurrentMarkerBox = ({marker, onChange, createPoint}) => (
    marker.isActive &&
    <div className="current-marker-box">
        <div className="container">
            <h2>Şikayet oluştur</h2>
            <div className="title form-input">
                <Input
                    placeholder="Şikayet başlığı"
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    value={marker.title}
                    name="title"
                    onChange={onChange}
                />
            </div>
            <div className="description form-input">
                <Input.TextArea
                    autosize={{ minRows: 2, maxRows: 10 }}
                    placeholder="Şikayet açıklaması"
                    value={marker.description}
                    name="description"
                    onChange={onChange}
                />
            </div>
            <div className="address">
                <div>Şikayet adresi</div>
                <span>{marker.address}</span>
            </div>
            <Button className="form-input" type="primary"
                onClick={() => createPoint(marker)}
            >Oluştur</Button>
        </div>
    </div>
)

export default CurrentMarkerBox;
