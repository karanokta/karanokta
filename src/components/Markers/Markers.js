import React from 'react';
import { Marker, InfoWindow } from 'react-google-maps';
import { Query } from 'react-apollo';
import { getPoints } from 'graphql/queries';

const Markers = (props) => (
    <Query query={getPoints}>
        {({loading, error, data: {getPoints}}) => {
            if (loading) return "Loading...";
            if (error) return `Error! ${error.message}`;
            return getPoints.map(marker => 
                <Marker
                    key={marker.id}
                    onClick={() => props.onClickMarker(marker.id)}
                    position={{lat: parseFloat(marker.latitude), lng: parseFloat(marker.longitude)}}>
                    {
                        props.activeWindowId === marker.id && <InfoWindow>
                            <div>
                                <div>{marker.title}</div>
                            </div>
                        </InfoWindow>
                    }
                </Marker>
            )
        }}
    </Query>
);

export default Markers;
