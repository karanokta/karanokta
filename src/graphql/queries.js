import gql from 'graphql-tag';

export const getPoints = gql`
    {
        getPoints{
            id
            title
            latitude
            longitude
            description
        }
    }
`

export const getPointDetail = gql`
    query($latitude: Float!, $longitude: Float!){
        getPointDetail(latitude: $latitude, longitude: $longitude){
            latitude
            longitude
            address
        }
    }
`
