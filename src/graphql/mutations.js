import gql from 'graphql-tag';

export const createPoint = gql`
    mutation($title: String!, $latitude: Float!, $longitude: Float!, $description: String!){
        createPoint(title: $title, latitude: $latitude, longitude: $longitude, description: $description){
            id
            title
            latitude
            longitude
            description
        }
    }
`
