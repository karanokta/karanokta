import React, { Component} from 'react';
import 'styles/Main.css';

import { graphql, withApollo, compose } from 'react-apollo'
import { message } from 'antd';

import { getPointDetail, getPoints } from 'graphql/queries'
import { createPoint } from 'graphql/mutations'

import { Map } from 'containers';
import { CurrentMarkerBox } from 'components';

class Main extends Component {
    constructor(props){
        super(props);
        this.state = {
            currentMarker: {
                title: "",
                description: "",
                address: "",
                isActive: false
            },
            activeWindowId: null,
        }
    }
    selectPoint = (data) => {
        const latitude = data.latLng.lat(), longitude = data.latLng.lng();
        this.props.client.query({
            query: getPointDetail,
            variables: {
                latitude,
                longitude
            }
        }).then(({data: {getPointDetail}}) => {
            if(getPointDetail){
                if(!getPointDetail.address){
                    message.error("Herhangi bir adres bulunamadı.")
                }else{
                    this.setState({
                        currentMarker: {
                            ...this.state.currentMarker,
                            latitude,
                            longitude,
                            address: getPointDetail.address,
                            isActive: true
                        }
                    })
                }
            }
        })

    }
    onClickMarker = (id) => {
        this.setState({activeWindowId: id})
    }
    onChangeCurrentMarker = (e) => {
        this.setState({
            currentMarker: {
                ...this.state.currentMarker,
                [e.target.name]: e.target.value
            }
        })
    }
    createPoint = ({title, description, latitude, longitude}) => {
        this.props.createPoint({
            variables: {
                title,
                description,
                latitude,
                longitude
            },
            update: (store, mutationResponse) => {
                if(mutationResponse.data){
                    var createdPoint = mutationResponse.data.createPoint;
                    var points = store.readQuery({query: getPoints});
                    points.getPoints = [...points.getPoints, createdPoint];
                    store.writeQuery({query: getPoints, data: points});
                }
            }
        }).then(res => {
            if(res.data){
                message.success("Şikayet başarıyla oluşturuldu!");
                this.setState({
                    currentMarker: {
                        title: "",
                        description: "",
                        address: "",
                        isActive: false
                    }
                });
            }
        })
    }
    render() {
        return (
            <div className="main">
                <CurrentMarkerBox
                    marker={this.state.currentMarker}
                    onChange={this.onChangeCurrentMarker}
                    createPoint={this.createPoint}
                />
                <Map selectPoint={this.selectPoint}
                    onClickMarker={this.onClickMarker}
                    activeWindowId={this.state.activeWindowId}
                    currentMarker={this.state.currentMarker}
                />
            </div>
        );
    }
}

export default compose(withApollo,
    graphql(createPoint, {name: "createPoint"})
)(Main);
