import React from 'react';

import { compose, withProps, withHandlers } from 'recompose';
import { GoogleMap, withGoogleMap, withScriptjs } from "react-google-maps";
import { MarkerClusterer } from "react-google-maps/lib/components/addons/MarkerClusterer";
import mapStyles from 'utils/mapStyles';
import { GoogleMap_API_KEY } from 'utils/config';

import { Markers, CurrentMarker } from 'components';

import 'styles/Map.css';

const Map = compose(
    withProps({
        googleMapURL: `https://maps.googleapis.com/maps/api/js?v=3.exp&language=tr&region=TR&key=${GoogleMap_API_KEY}`,
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: '100vh' }} />,
        mapElement: <div style={{ height: `100%` }} />,
    }),
    withHandlers({
        //
    }),
    withScriptjs,
    withGoogleMap)(props => <GoogleMap
            defaultZoom={8}
            defaultOptions={{styles: mapStyles, disableDefaultUI: true}}
            onClick={props.selectPoint}        
            defaultCenter={{ lat: 41.022020, lng: 28.963059 }}>
            <MarkerClusterer
                averageCenter
                enableRetinaIcons
                gridSize={60}>
                <Markers onClickMarker={props.onClickMarker} activeWindowId={props.activeWindowId} />
                <CurrentMarker currentMarker={props.currentMarker} />
            </MarkerClusterer>
        </GoogleMap>
    );


export default Map;
