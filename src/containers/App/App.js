import React, { Component } from 'react';

import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { Main } from 'containers'
import 'antd/dist/antd.css';
import { graphQLPoint } from 'utils/config';

const client = new ApolloClient({
    uri: `http://${graphQLPoint}/graphql`
});

class App extends Component {
    render() {
        return (
            <ApolloProvider client={client}>
                <Main />
            </ApolloProvider>
        );
    }
}

export default App;
